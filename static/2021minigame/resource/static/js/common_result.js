/**
 * @description 判断是否在JOOX内
 * @returns {boolean}
 */
 function isJoox() {
  // var UserAgent = navigator.userAgent.toLowerCase();
  // return /tencent-joox/i.test(UserAgent);
  return !!wmid;
}
/**
 * @description 获取url的某个参数
 * @param {string} url
 * @param {string} name
 * @returns {string} 对应name的值
 */
function getParameter(url, name) {
  var r = new RegExp(`(\\?|#|&)${name}=([^&#\\?]*)(&|#|$|\\?)`, "i");
  var m = url.match(r);
  if (!m || m === "") {
    m = top.location.href.match(r);
  }

  return !m ? "" : m[2];
}

/**
 * @description get请求函数
 * @param {string} url 请求
 * @param {function} successCallback 成功回调函数
 * @param {function} errorCallback 失败回调函数
 */
 function get(url, successCallback, errorCallback) {
  var xmlhttp;
  // compatible with IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4) {
      if (xmlhttp.status == 200) {
        successCallback(xmlhttp.responseText);
      } else {
        if (typeof errorCallback == "function") {
          errorCallback();
        }
      }
    }
  };
  xmlhttp.open("GET", url + "&cache=" + new Date().getTime(), true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send();
}

/**
 * @description post请求函数
 * @param {string} url 请求
 * @param {string} params 经过JSON.stringify处理过的请求参数
 * @param {function} successCallback 成功回调函数
 * @param {function} errorCallback 失败回调函数
 */
function post(url, params, successCallback, errorCallback) {
  var xmlhttp;
  // compatible with IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4) {
      if (xmlhttp.status == 200) {
        successCallback(xmlhttp.responseText);
      } else {
        if (typeof errorCallback == "function") {
          errorCallback();
        }
      }
    }
  };
  xmlhttp.open("POST", url + "?cache=" + new Date().getTime(), true);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send(params);
}
/**
 * 通用调用 JOOX JSAPI 方法
 * @param {string} method 调用方法
 * @param {*} [params] 调用参数
 * @param {Function} [callback] 回调函数
 */
 function invoke(method, params, callback) {
  if (typeof window.JooxJavascriptBridge === 'object' && typeof window.JooxJavascriptBridge.invoke === 'function') {
    window.JooxJavascriptBridge.invoke(method, params, function (res) {
      callback && callback(res);
    });
  }
}
function registerHandler(handlerName, handler) {
  if (typeof window.JooxJavascriptBridge === 'object' && typeof window.JooxJavascriptBridge.registerHandler === 'function') {
    try {
      window.JooxJavascriptBridge.registerHandler(handlerName,
        function (responseData) {
          handler && handler(responseData);
        });
    } catch (e) {}
  } else {
    console.log('注册失败', handlerName, typeof window.JooxJavascriptBridge, typeof window.JooxJavascriptBridge.registerHandler);
  }
}

/**
 * 兼容ios端的jsapi调用
 * @param {String} type 
 */
function invokeJsBridge(type) { 
  invoke(type);
  document.addEventListener("JooxJavascriptBridgeReady", function () {
      invoke(type);
  }, true);
  window.onload = function () { 
      invoke(type);
  }
}

var url = location.href;
var wmid = encodeURIComponent(getParameter(url, location.search.indexOf('sharefromwmid') > 0 ? "sharefromwmid" : "wmid"));
var isJOOX = isJoox();
var doc = document;
var openudid = encodeURIComponent(getParameter(url, "openudid"));
var key = encodeURIComponent(getParameter(url, "key").replace(/\s/g, "+"));
var area = encodeURIComponent(getParameter(url, "area"));
var lang = encodeURIComponent(getParameter(url, "lang"));
var nickname = "";
var taskid = "";
var taskidStatus = {};

// 获取域名
var CGIDOMAINS = {
  "www.joox.com": "https://www.voovlive.com",
  "test.web.joox.com": "https://test.voovlive.com"
};
var JOOXDOMAIN = location.host || "www.joox.com";
var CGIDOMAIN = CGIDOMAINS[JOOXDOMAIN] || "https://www.voovlive.com";

// 关闭minibar
invokeJsBridge("closeMinibar");
// 关闭音乐
invokeJsBridge("pauseMusic");
// 获取用户信息
// getUserInfo();

// 分享回调
if (!window.isIos) {
  console.log('invokeSharePageInteraction')
  // invokeSharePageInteraction();
  registerSharePageInteraction(function (res) {
    console.log('registerSharePageInteraction', res)
    if (res.code == '0' || res.ret == 0) {
      var taskids = '10791'
      isJOOX && getTaskStatus(taskids);
    }
  })
}


if (window.DOMContentLoaded == 1) {
  initPage();
} else {
  doc.addEventListener("DOMContentLoaded", initPage, true);
}
function initPage() {
  if (!lang) lang = window._language || 'en';
  // 上报
  try {
    H5REPORT.init();
  } catch (e) {
    console.log(e)
  }
  try {
    H5REPORT.pageview();
  } catch (e) {
    console.log(e)
  }

  // 查询任务状态
  // isJOOX && getTaskStatus();

  // 微信二次分享
  // !isJOOX && setShareData();
  setShareData();
  // setUserHead();

}

function getReqDomain() {
  //点击按钮
  var domains = {
    "www.joox.com": "api.joox.com",
    "test.web.joox.com": "test.api.joox.com",
    // "www.joox.com": "dev.joox.com"
  };

  return domains[location.host] || "api.joox.com";
}

// function getUserInfo () {
//   var reqUrl =
//     "https://" +
//     getReqDomain() +
//     "/oper-cgi/get_uinfo?wmid=" +
//     wmid +
//     "&country=" +
//     area +
//     "&openudid=" +
//     openudid +
//     "&key=" +
//     key +
//     "&fields=nickname"
//   get(reqUrl, function (res) {
//     console.log('getUserInfo', reqUrl, res, JSON.stringify(res))
//     var result = JSON.parse(res);
//   });
// }

function setUserHead() {
  var reqUrl =
    CGIDOMAIN +
    "/joox_http_kwork/web/user/basic_info?wmid=" +
    wmid +
    "&openudid=" +
    openudid +
    "&key=" +
    key;

    console.log('setUserHead', reqUrl)
  get(
    reqUrl,
    function(res) {
      console.log('setUserHead', res)
      try {
        var result = JSON.parse(res);
        if (result.code === 0) {
          var data = result.data;
          var head = data.memberInfo.sTransferHeadUrl;
          head = head ? head.replace("%d", 156) : "";
          if (head) window._setGameUserHead(head);
        }
      } catch (err) {
        window._setGameUserHead(null);
        console.log('setUserHead catch', err);
      }
    },
    function(err) {
      console.log('setUserHead err', err);
      window._setGameUserHead(null);
    }
  );
}

function getTaskId() {
  var taskids = {
    hk: 11118,
    mo: 11118,
    th: 11121,
    id: 11120,
    my: 11119,
    mm: 11122
  };
  taskid = taskids[area || "hk"];

  return taskid;
}

function getMessages () {
  var msgs = {
    'en': {
      'rewards': {
        'button': 'Get VIP now',
        '0': 'get reward success',
        '-14': '你已领取该分段奖励，继续玩到更高分吧',
        'fail': 'get reward fail'
      },
      'share': {
        '0': '分享成功，成功领取vip奖励',
        '-14': '你已领取分享奖励',
      }
    },
    'zh_CN': {
      'rewards': {
        'button': '',
        '0': '成功领取vip奖励',
        '-14': '你已领取该分段奖励，继续玩到更高分吧',
        'fail': '领取失败'
      },
      'share': {
        '0': '分享成功，成功领取vip奖励',
        '-14': '你已领取分享奖励',
      }
    }
  }
  return msgs;
}

function getTaskMessage () {
  var msgs = getMessages();
  var taskMsgs = {
    '10791': {
      '0': msgs[lang].share['0'],
      '-14': msgs[lang].share['-14']
    },
  }
  taskMsgs['11537'] = taskMsgs['11538'] = taskMsgs['11539'] = {
    'fail': msgs[lang].rewards['fail'],
    '0': msgs[lang].rewards['0'],
    '-14': msgs[lang].rewards['-14']
  }
  return taskMsgs;
}

function removeRewardsBtnGrey () {
  var msgs = getMessages();
  document.querySelector('#getRewardsBtn').classList.remove('grey');
  document.querySelector('#getRewardsBtn').innerHTML = msgs[lang].rewards.button;
}

function getTaskStatus(taskids, success) {
  var reqUrl =
    "https://" +
    getReqDomain() +
    "/oper-cgi/get_task_status?wmid=" +
    wmid +
    "&taskids=" +
    (taskids || getTaskId()) +
    "&country=" +
    area;
  // invoke('get', { url: reqUrl }, taskSuccess);
  console.log('getTaskStatus', taskids, reqUrl)
  get(reqUrl, success || taskSuccess, taskFail);
}
/**
 {errmsg: "", retcode: 0}
retcode的值对应该任务领取的状态
 0 成功
 -2 系统错误
 -3 频率控制
 -4 参数错误
 -10 任务未开始
 -11 任务结束
 -12 不存在
 -13 已达上限
 -14 用户已完成
 -15 其他限制 
 */
function taskSuccess(res) {
  // {"errmsg":"","result":{"10168":0,"10382":-12,"10389":-12},"retcode":0} result代表任务id对应的状态
  try {
    var result = JSON.parse(res);
    // var result = res;
    console.log('taskSuccess', res, result, 'result', JSON.stringify(result.result))
    if (result.retcode === 0 || result.ret === 0) {
      var status = result.result;
      taskidStatus = status
      var keys = Object.keys(result.result)
      for (var i = 0; i < keys.length; i++) {
        var taskid = keys[i]
        if (result.result[taskid] === 0) {
          getVip(taskid);
          break
        } else if (result.result[taskid] === -14) {
          console.log("get task finish");
          var taskMsgs = getTaskMessage();
          popupMessage(0, taskMsgs[taskid]['-14']);
          break
        } else {
          console.log("get task err");
          popupMessage(0, taskMsgs[taskid][result.result[taskid]] || 'get rewards error');
          break
        }
      }
    } else {
      console.log("get task err");
      removeRewardsBtnGrey();
    }
  } catch (e) {
    console.log("get task status err");
    removeRewardsBtnGrey();
  }
}

function taskFail(err) {
  console.log('taskFail', err);
  removeRewardsBtnGrey();
}

function getVip(taskid) {
  var reqUrl = "https://" + getReqDomain() + "/oper-cgi/click_event_handler";

  var params = {
    uid: wmid,
    taskid: taskid || getTaskId(),
    country: area
  };
  console.log('getVip', params)
  // invoke('post', params, getVipSuccess);
  post(reqUrl, JSON.stringify(params), function (res) {
    getVipSuccess(res, taskid)
  }, getVipFail);
  
}

function getVipSuccess(res, taskid) {
  try {
    if (typeof res == 'string') {
      res = JSON.parse(res);
    }
    console.log('getVipSuccess', JSON.stringify(res))
    var result = res;
    if (result.retcode === 0 || result.ret === 0) {
      console.log("getVipSuccess");
      var taskMsgs = getTaskMessage();
      console.log("getVipSuccess taskMsg", taskMsgs)
      // window.showToast(0, taskMsgs[taskid]['send']);
      popupMessage(1, taskMsgs[taskid]['0']);
    } else {
      console.log("get vip fail", result.retcode, result.ret);
      popupMessage(2, taskMsgs[taskid]['fail']);
      removeRewardsBtnGrey();
    }
  } catch (e) {
    popupMessage(2, taskMsgs[taskid]['fail']);
    console.log("get vip err");
    removeRewardsBtnGrey();
  }
}

function getVipFail(err) {
  console.log('getVipFail', err);
  popupMessage(2, taskMsgs[taskid]['fail']);
  removeRewardsBtnGrey();
}

/**
 * 装了JOOX就唤醒APP，没装提示去下载
 */
function openJOOX() {
  H5REPORT.send({ page: "2021minigame", action: 2, item: "btn_openjoox" });
  var schemeUlr =
    "page=innerweb&innerweburl=" +
    encodeURIComponent(
      "https://" +
        location.host +
        "/redirect.html?page=2021minigame&lang=" + lang + "&needkey=1&out=1&from=html5page&platform=other"
    );
  var json = {
    androidArgs: "&" + schemeUlr,
    iosArgs: "&" + schemeUlr,
    GA_category: "2021minigame"
  };
  if (isJooxInstalled) {
    openBox("", json);
  } else {
    var downloadBoxEl = doc.querySelector(".box-download");
    if (!downloadBoxEl) {
      new downloadBox(
        "//www.joox.com/en_my/app/img/playlist-bg.jpg",
        null,
        "2021minigame",
        json
      );
    } else {
      downloadBoxEl.style.display = "block";
    }
  }
}

/**
 * 设置微信二次分享数据
 */
 function setShareData() {
   
  var score = getParameter(url, "score");
  if (score) {
      score = Number(score);
  }
  var titles = {
      en: "JOOX Birthday! Go to play game and get JOOX VIP~",
      th: "เล่นเกมส์ลุ้นรางวัล ฉลอง JOOX 5 ปี",
  };
  var descs = {
    en: "How many points can you get?Let's try!",
    th: "มาทดสอบความเทพกับเกมส์สุดหินกันเถอะ!",
  };

  shareData.ok = 1;
  shareData.img_width = "300";
  shareData.img_height = "300";
  shareData.img_url = "https://" + JOOXDOMAIN + "/static/2021minigame/resource/" +
  lang  +
  "/assets/Share/350x350.png";
  shareData.link = "https://" + JOOXDOMAIN + '/' +
  lang  +
  '/operation/2021minigame/index.html?activityid=10791&sharefromwmid=' + wmid;
  shareData.friendLink = shareData.link;
  shareData.friendCycleLink = shareData.link;
  shareData.desc = descs[lang];
  shareData.title = titles[lang];
  shareData.GA_category = "2021minigame";
  var platformCode,
    ua = navigator.userAgent;
  if (/iphone/i.test(ua)) {
    platformCode = "ios";
  } else if (/android/i.test(ua)) {
    platformCode = "android";
  }
  shareData.platform = platformCode;
  document.querySelector('meta[name="joox-share-link"]').setAttribute('content', shareData.link);
  document.querySelector('meta[property="og:url"]').setAttribute('content', shareData.link);
}

function registerSharePageInteraction(handler) {
  registerHandler('sharePageInteraction', function (responseData) {
      handler && handler(responseData);
  });
}
function invokeSharePageInteraction() {
  invoke('sharePageInteraction', {}, function (res) {
    console.log('sharePageInteraction', res)
    if (res.code == '0' || res.ret == 0) {
      var taskids = '10791'
      isJOOX && getTaskStatus(taskids);
    }
  });
}

function sharePage() {
  var params = {
    title: shareData.title,
    description: shareData.desc,
    imageurl: shareData.img_url,
    link: shareData.link,
    activityid: 10791,
    shareTarget: 3935,
    shareTargetConfig: 3935,
  }
  console.log('sharePage', params)
  invoke('sharePage', params);
  window.isShare = true;
}

/**
 * 弹出tips消息框
 * @memberof jsapi
 * @method jsapi#popupMessage
 * @param {number} type=0 0默认普通窗口(白色圆圈+i)，1成功信息窗口(绿色圆圈+√)，2失败信息窗口(红色圆圈+！)
 * @param {string} text='' 窗口信息
 * @since 3.0
 */
function popupMessage(type = 0, text = '') {
  const params = {
      type,
      text,
  }; 
  console.log("popupMessage", params)
  invoke('tipsMessage', {
      params,
  });
}

if (!Object.keys) {
  Object.keys = (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
        dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ],
        dontEnumsLength = dontEnums.length;

    return function (obj) {
      if (typeof obj !== 'object' && typeof obj !== 'function' || obj === null) throw new TypeError('Object.keys called on non-object');

      var result = [];

      for (var prop in obj) {
        if (hasOwnProperty.call(obj, prop)) result.push(prop);
      }

      if (hasDontEnumBug) {
        for (var i=0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) result.push(dontEnums[i]);
        }
      }
      return result;
    }
  })()
};

(function() {
  function Toast() {};
  Toast.prototype = {
      init:function(){
        
      },
      iconObj : {
          right:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAADIElEQVRYR+2XT0jUQRTH3/e3u0YSYSDuzgwUCOHBS4e6BJmEEfRfiDAI6aAEYkTgsahDNy+BhUQgEgbRoZCoiEpSokuXLh26CAUz4/5YECSIdH++GHFjG9f9o9tq4Bxn5r33+X3fm/d2QZt0YZNy0RZYpZnZUuy/VSwMw71RFF0goqlUKjW5KVJprT3AzG+JaOeystc3HCydTh+Moug1Ee3IpZuZJzYUzFrbvri4+BLA9vwaZObeDQPTWh8loucAtnlQV5RSdzcEzFp7gpmfEVEiD4qJ6LKU8oHbqzmYMaaTiJ4QUTwfCsAlIcTD3F5NwbTWXQDGiChWDKqmillru5l51MtSxMwXlVKP/QZcE8VWgcoS0Xkppau1FQthGKay2ewoM7cCuCqlfFrp+Ch23xjTS0T3PaWyAM4KIV6sZgut9TiA08sXmJl7lFIj1YDTWvcDGPJ8LQDoLAa1VGNaa9dLTuYbA+gTQgyvB85aO8DMg16P+kVEp5RSb0r5hjFmPzNP+d2XiG5IKW+XclDoXGt9C8BND+pnEATHhRDvy/G5VPyF5tWy8R0p5bVyHOXuaK0HAQz4UPF4vCOZTH4s19efV1lgwud8DEsp+8pxqLUeAtDv3f0Ri8WOVQK1oo+FYbhvYWFhAsAu74tHpJQ9ANzYWLGYGdZa9/LcC8xfcwA6hBCfyvmwv+rcN7DWtjKzq4NG7+yREKIbwKIH7aBc4+z29mcTicSRpqamz5VCrVAsr05aAHwoADcuhDgHwDVHYuaYMWYMQJcPFQTBISHEl7VArQrmDrTWDm6SiJKe81dCiDOOy1rrhrEbyvkrA6B9PVBFwdzhzMxMcxRFrpUoT5F3AOYKQKWZ+bBS6utalcrZlZyVxpg97g8CEe0uESwdBMHBVCo1vV6okorlAmQyGTU/P+/gmgsFZWYdi8XaqgVVNpi7mE6nk9lsdhJAiwf3nYjapJTfqqFU2anMD2aMcS3EtZLW5f3purq6tsbGRl1NqIoUywVm5jr3m93B1dfX32toaJitNtSawP4FRCGfJV9lrUD8OFtglSq/pViliv0GWXY44L2QOz4AAAAASUVORK5CYII=',
          err:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAACy0lEQVRIS7XXSWsUQRgG4PfrHiOIJw9DLz/BqyKRmMgQcQnGuBACooLgQS96Ebx6EW/eBEVBCC6IMS6oEbeLCCKI4MU/UFUDOUgkCppMv1KhWzplz3RPkplb18zUU+9XS3eLMWYzyfMALkdR9A09/BhjtiZJcsbzvIuilPogIv0AftRqtaF6vf6lF7YxZmeSJDMish7AlBhjTpC8lWI9wR20JSKjYkGt9SkA13uBO+gigPEoiqaX4F7hSqldAJ6m5V0UkbEwDJ9Z7x+cw6+l7asqu0VFxCLrACxD/4NtgzHmeDrndlArwsvQQrgIF5HhMAw/VVntVdC2cAE+LyKNMtxBF0TkYDan7qCXzbH7pVP2jrgxZoTkdDqnCyRH4jh+1a5KHeGqyVP0EYAagFK0Y6nzI+2UfCVoZbhdcgB1kl0lzQKVljqfXCk1ISJ30n3+E4A9dyuXN99XV7D9o4OD5G8A+zstpKIF1jVsjDlC8n526lnY9/19QRC8rbLPV1TqFL0HwAfwC0CfLbXF7SETRdH7qnjlxEqpURF5mEN3k9zktlXFK8EpOlWUzq2C53mNIAg+liUvhbXWe+ytLUOL5tOZ93nP84bL8I6wRUk+FpE+kn9839/bbhFprY8CmEwXXSneFnZRETkQRdFMpxK6OMmhOI4/V95OzWaz0Wq1Xtik9iZu92kZmnWulDopIjfS5HMkG0X4f4lT9Hn2uELycBzHT8oWi3PCWfxm2jbned6OIAi+tj25tNYDJF+vBs06N8acJnnVXpP87vv+UB7PP+wNAHgJYAOAFslD3SYtuJ8vw0Vke/bSsAQ3m81tSZLYI28JFZGJMAwfdFPedr/NJwcwC2DQ4qK13gLgHYCNtioiMr5WaDYYrfU5AFfS61nf9/vtK8wb+zxlUQDHoii6vRZJ3T4cfMomHgRwgeRkHMd3e4Hmkp8FMEby0l8qK9qQ0A6pwwAAAABJRU5ErkJggg==',
  tip:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAABrUlEQVRIS+3WMUsDMRQA4PcKtlBQiu1J76UFpUgHVyd/gJs4iri4Cro4SxXBWdBFHARBHJxd/AUOrg4i0sVe2iMVcY69SCTDVSi9HDZdmuXg7iVfkktegjCmgmNyYQInmvkgCKqI2AAACQANIuomqhgLSjXVnPNnAFjS7Sil3hhjiyOHhRC+lJLHoVwuVy0Wiy0b3HrEnU5nLoqiMI7k8/nZQqHwOVJYKYXtdjuKIYqIMjaojrUesa7EOf8CgBmDdYnIcwU3AWDBLK5XxljdFfwEAMsGfmSMrbiCHwBg1cD3jLE1V/AtAGwa7JqItp3AQRCcI+KuwU6JaN8VfISIhwY7IKITV/AeIp4ZbIeILpzAnPMtALj5TQSIG77v3zmBwzCs9Xq9F41lMpl6uVzW+9qqpMpcWhBCkH56ntd3YCTVU8NJgUFxqWAhxLSUch0Ra1EUXTHG3m07kgr+cxFoEdE8IvZscGtY/1spZRBHstlspVQq9b0b1glrWCk1xTlvImLF5OoPIioj4vcwLP7dGjYrWl9/js0+vvR9X59WViUVbCUMCJ7A/zGLidr4AdwkmB8lttWIAAAAAElFTkSuQmCC'
      },
      toastTxtObj : {
          right:'成功',
          err:'失败',
          tip:'提示'
      },
      /*
      * icon 1成功   2失败  3 提示
      * toastTxt 提示语
      * time 关闭前的毫秒数 不传默认2000
       */
      showToast: function(icon,toastTxt,time){
        var iconSrc = null;        
        switch(parseInt(icon)){
            case 1:
            iconSrc = this.iconObj.right;
            break;
            case 2:
            iconSrc = this.iconObj.err;
            break;
            case 3:
            iconSrc = this.iconObj.tip;
            break;
            default:
            break;
        }
        var toastTxt = toastTxt || this.toastTxtObj.right;
        html = (iconSrc ? '<div class="toast-icon"><img src="'+iconSrc+'"></div>' : '') + '<div class="toast-tip">'+toastTxt+'</div></div>';
        toastBox = document.createElement('div');
        toastBox.className = 'toast';
        toastBox.id = 'toast';
        toastBox.innerHTML = html;
        document.body.appendChild(toastBox);
        var toastTip = toastBox.querySelector('.toast-tip');
        var toastTipWidth = toastTip.offsetWidth;
        var toastTipMaxWidth = window.innerWidth * 0.8 - 24;
        if (toastTipWidth > toastTipMaxWidth) {
          toastTipWidth = toastTipMaxWidth;
        }
        toastTipWidth += 2;
        console.log(toastTip, toastTipWidth)
        toastBox.style.width = (toastTipWidth + 24) + 'px';
        toastBox.classList.add('show');
        setTimeout(function(){
          document.getElementById('toast').parentNode.removeChild(document.getElementById('toast'));
        },time||2500)
      },
  
  
  
  }
  var T = new Toast();
  T.init();
  window.showToast = T.showToast;
})();