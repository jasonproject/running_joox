var doc = document,
  isJooxInstalled = false,
  checked = false,
  shareData = {},
  openInfo = {};

// 分享给朋友
function onShare2Friends() {
  if (shareData && shareData.ok === 1) {
    ga(
      "send",
      "event",
      shareData.GA_category,
      shareData.platform,
      "share2Friends"
    );
    WeixinJSBridge.invoke(
      "sendAppMessage",
      {
        img_url: shareData.img_url,
        img_width: shareData.img_width,
        img_height: shareData.img_height,
        link: shareData.link || shareData.friendLink,
        desc: shareData.desc,
        title: shareData.title
      },
      function(ex) {}
    );
  }
}

// 分享到朋友圈
function onShare2FriendCycle() {
  if (shareData && shareData.ok === 1) {
    ga(
      "send",
      "event",
      shareData.GA_category,
      shareData.platform,
      "share2FriendCycle"
    );
    WeixinJSBridge.invoke(
      "shareTimeline",
      {
        img_url: shareData.img_url,
        img_width: shareData.img_width,
        img_height: shareData.img_height,
        link: shareData.link || shareData.friendCycleLink,
        desc: shareData.desc,
        title: shareData.title
      },
      function(ex) {}
    );
  }
}

// 分享事件绑定
function onShareEvent() {
  WeixinJSBridge.on("menu:share:appmessage", onShare2Friends);
  WeixinJSBridge.on("menu:share:timeline", onShare2FriendCycle);
}

// 检查是否安装Joox客户端
function checkIntall() {
  if (
    typeof WeixinJSBridge === "object" &&
    typeof WeixinJSBridge.invoke === "function"
  ) {
    WeixinJSBridge.invoke(
      "getInstallState",
      {
        packageUrl: "wemusic://",
        packageName: "com.tencent.ibg.joox"
      },
      function(res) {
        if (res.err_msg.split(":")[1] != "no") {
          isJooxInstalled = true;
        } else {
          var btnOpen = doc.getElementById("open");
          var bigOpen = doc.getElementById("btnOpen");
          if (btnOpen) {
            btnOpen.innerHTML = '{#= _("安装")#}';
          }
          if (bigOpen) {
            bigOpen.innerHTML = '{#= _("安装JOOX")#}';
          }
        }

        var ref =
          typeof getParameter === "function" &&
          getParameter(location.href, "ref");
        if (ref === "qrcode") {
          document.getElementById("open").innerHTML = '{#= _("前往JOOX")#}';
        }

        typeof window.wechatCallback === "function" &&
          window.wechatCallback(isJooxInstalled);
      }
    );
  }

  checked = true;
}

function cmpVersion(a, b) {
  var i,
    cmp,
    len,
    re = /(\.0)+[^\.]*$/;
  a = (a + "").replace(re, "").split(".");
  b = (b + "").replace(re, "").split(".");
  len = Math.min(a.length, b.length);
  for (i = 0; i < len; i++) {
    cmp = parseInt(a[i], 10) - parseInt(b[i], 10);
    if (cmp !== 0) {
      return cmp;
    }
  }
  return a.length - b.length;
}

//scheme跳转在微信内被禁止，改用JSBridge跳转
function launchApp(scheme, ref) {
  scheme += "&reportChannel=2&reportType=1&reportDetail=";
  var wechatInfo = navigator.userAgent.match(/MicroMessenger\/([\d\.]+)/i);
  var canReport =
    typeof ga === "object" && typeof openInfo === "object" ? true : false;

  if (
    typeof WeixinJSBridge === "object" &&
    typeof WeixinJSBridge.invoke === "function" &&
    cmpVersion(wechatInfo[1], "6.5.6") >= 0
  ) {
    canReport &&
      ga(
        "send",
        "event",
        openInfo.GA_category,
        platform,
        "openClient_jsbridge"
      );
    WeixinJSBridge.invoke(
      "launchApplication",
      {
        schemeUrl: encodeURI(decodeURIComponent(scheme))
      },
      function(ex) {
        if (ex.err_msg.indexOf("ok") === -1) {
          canReport &&
            ga(
              "send",
              "event",
              openInfo.GA_category,
              platform,
              "openMarket_jsbridge"
            );
          location.href = "//www.joox.com/d?ref=" + ref;
        }
      }
    );
  } else {
    location.href = scheme;
  }
}

function openJoox(ref) {
  var downloadUrl = "http://www.joox.com/d?ref=" + ref,
    scheme = "wemusic://www.joox.com?from=html5page&platform=",
    ua = navigator.userAgent,
    platform = "other";

  var langCode,
    area =
      getParameter(location.href, "area") ||
      getParameter(location.href, "backend_country") ||
      getParameter(location.href, "country");

  if (area) {
    langCode =
      window.location.pathname.split("/")[1].substring(0, 2) +
      (area ? "_" + area : "");
  } else {
    langCode = window.location.pathname.split("/")[1];
  }

  downloadUrl = downloadUrl + (langCode ? "&lang=" + langCode : "");

  if (/android/i.test(ua)) {
    (scheme += "android" + openInfo.androidArgs), (platform = "android");
  } else if (/iphone/i.test(ua)) {
    (scheme += "ios" + openInfo.iosArgs), (platform = "iOS");
  }

  if (isJooxInstalled) {
    ga("send", "event", openInfo.GA_category, platform, "openClient");
    setTimeout(function() {
      launchApp(scheme);
    }, 200);
  } else {
    if (checked) {
      ga("send", "event", openInfo.GA_category, platform, "openMarket");
      setTimeout(function() {
        location.href = downloadUrl;
      }, 0);
    } else {
      checkIntall();
      var url = "";
      if (isJooxInstalled) {
        ga("send", "event", openInfo.GA_category, platform, "openClient");
        setTimeout(function() {
          launchApp(scheme);
        }, 200);
      } else {
        ga("send", "event", openInfo.GA_category, platform, "openMarket");
        url = downloadUrl;

        setTimeout(function() {
          location.href = url;
        }, 200);
      }
    }
  }
}

function init() {
  checkIntall();
  onShareEvent();
}

if (
  typeof WeixinJSBridge === "object" &&
  typeof WeixinJSBridge.invoke === "function"
) {
  init();
} else {
  if (doc.addEventListener) {
    doc.addEventListener("WeixinJSBridgeReady", init, false);
  } else if (doc.attachEvent) {
    doc.attachEvent("WeixinJSBridgeReady", init);
    doc.attachEvent("onWeixinJSBridgeReady", init);
  }
}
