/**
 * @description 判断是否在JOOX内
 * @returns {boolean}
 */
function isJoox() {
    //var UserAgent = navigator.userAgent.toLowerCase();
    //return /tencent-joox/i.test(UserAgent);
    return !!wmid;
}
/**
 * @description 获取url的某个参数
 * @param {string} url
 * @param {string} name
 * @returns {string} 对应name的值
 */
function getParameter(url, name) {
  var r = new RegExp(`(\\?|#|&)${name}=([^&#\\?]*)(&|#|$|\\?)`, "i");
  var m = url.match(r);
  if (!m || m === "") {
    m = top.location.href.match(r);
  }

  return !m ? "" : m[2];
}

function getReqDomain() {
  //点击按钮
  var domains = {
    "www.joox.com": "api.joox.com",
    "test.web.joox.com": "test.api.joox.com",
    // "www.joox.com": "dev.joox.com"
  };

  return domains[location.host] || "api.joox.com";
}

/**
 * 通用调用 JOOX JSAPI 方法
 * @param {string} method 调用方法
 * @param {*} [params] 调用参数
 * @param {Function} [callback] 回调函数
 */
 function invoke(method, params, callback) {
  if (typeof window.JooxJavascriptBridge === 'object' && typeof window.JooxJavascriptBridge.invoke === 'function') {
    window.JooxJavascriptBridge.invoke(method, params, function (res) {
      callback && callback(res);
    });
  }
}

/**
 * 兼容ios端的jsapi调用
 * @param {String} type 
 */
function invokeJsBridge(type) { 
  invoke(type);
  document.addEventListener("JooxJavascriptBridgeReady", function () {
      invoke(type);
  }, true);
  window.onload = function () { 
      invoke(type);
  }
}
function getTaskId() {
  var taskids = {
    hk: 10820,
    th: 10791,
    en: 10791,
  };
  taskid = taskids[area || "hk"];

  return taskid;
}

var doc = document;
var url = location.href;
var wmid = encodeURIComponent(getParameter(url, "wmid"));
var area = encodeURIComponent(getParameter(url, "area"));
var lang = encodeURIComponent(getParameter(url, "lang"));
var openudid = encodeURIComponent(getParameter(location.href, "openudid"));
var key = getParameter(location.href, "key");
key = key.replace(/\s/g, "+");
var taskid = "";
var isJOOX = isJoox();

// 获取域名
var CGIDOMAINS = {
  "www.joox.com": "https://www.voovlive.com",
  "test.web.joox.com": "https://test.voovlive.com"
};
var JOOXDOMAIN = location.host || "www.joox.com";
var CGIDOMAIN = CGIDOMAINS[JOOXDOMAIN] || "https://www.voovlive.com";

// 设置默认头像
var defaultHead =
  "https://" + JOOXDOMAIN + "/static/image/ksong_tournament/default.jpg";

// 关闭minibar
invokeJsBridge("closeMinibar")
// 关闭音乐
invokeJsBridge("pauseMusic")
if (window.DOMContentLoaded == 1) {
  initPage();
} else {
  doc.addEventListener("DOMContentLoaded", initPage, true);
}

function initPage() {
  if (!lang) lang = window._language || 'en';
  // 上报
  try {
    H5REPORT.init();
  } catch (e) {
    console.log(e)
  }
  try {
    H5REPORT.pageview();
  } catch (e) {
    console.log(e)
  }
  // 微信二次分享
  !isJOOX && setShareData();
  !isJOOX && H5REPORT.send({ page: "2021minigame", action: 1, item: "2021game_share" });
}

/**
 * 装了JOOX就唤醒APP，没装提示去下载
 */
function openJOOX() {
  H5REPORT.send({ page: "2021minigame", action: 2, item: "btn_openjoox" });
  var schemeUlr =
    "page=innerweb&innerweburl=" +
    encodeURIComponent(
      "https://" +
        JOOXDOMAIN +
        "/redirect.html?page=2021minigame&lang=" + lang + "&needkey=1&out=1"
    );
  var json = {
    androidArgs: "&" + schemeUlr,
    iosArgs: "&" + schemeUlr,
    GA_category: "2021minigame"
  };
  if (isJooxInstalled) {
    openBox("", json);
  } else {
    var downloadBoxEl = doc.querySelector(".box-download");
    if (!downloadBoxEl) {
      new downloadBox(
        "//www.joox.com/en_my/app/img/playlist-bg.jpg",
        null,
        "2021minigame",
        json
      );
    } else {
      downloadBoxEl.style.display = "block";
    }
  }
}

/**
 * 设置微信二次分享数据
 */
function setShareData() {
  var titles = {
    en: "JOOX Birthday! Go to play game and get JOOX VIP~",
    id: "Fun March in JOOX",
    ms: "Buru Hadiah dan Menangi VIP!",
    my: "JOOX VIP ရရှိဖို့ Gift ဖမ်းသူဖြစ်လိုက်ပါ !",
    th: "เล่นเกมส์ลุ้นรางวัล ฉลอง JOOX 5 ปี",
    zh_CN: "JOOX VIP大作战",
    zh_TW: "「3日VIP」Challenge！"
  };
  var descs = {
    en: "How many points can you get?Lets try!",
    id: "Mainkan gamenya dan menangkan JOOX VIP!",
    ms: "Main permainan ini untuk peluang mendapatkan ganjaran VIP.",
    my: "ပျော်ရွှင်စရာပဲ ! VIP ရရှိဖို့ ဂိမ်းကိုအနိုင်ကစားပါ !",
    th: "มาทดสอบความเทพกับเกมส์สุดหินกันเถอะ!",
    zh_CN: "挑战游戏赢取VIP 奖励",
    zh_TW: "立即挑戰「3日VIP」Challenge！成功挑戰送你3日VIP服務體驗！"
  };

  shareData.ok = 1;
  shareData.img_width = "300";
  shareData.img_height = "300";
  shareData.img_url = "https://" + JOOXDOMAIN + "/static/2021minigame/resource/" +
  lang  +
    "/assets/Share/350x350.png";
  shareData.link = "https://" + JOOXDOMAIN + '/' +
  lang  +
  '/operation/2021minigame/index.html?activityid=' + getTaskId() + '&sharefromwmid=' + wmid;
  shareData.friendLink = location.href;
  shareData.friendCycleLink = location.href;
  shareData.desc = descs[lang];
  shareData.title = titles[lang];
  shareData.GA_category = "2021minigame";
  var platformCode,
    ua = navigator.userAgent;
  if (/iphone/i.test(ua)) {
    platformCode = "ios";
  } else if (/android/i.test(ua)) {
    platformCode = "android";
  }
  shareData.platform = platformCode;
}
