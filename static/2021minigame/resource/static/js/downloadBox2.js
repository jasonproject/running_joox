/*
    bgImageUrl: String, 浮层背景图片url
    clickEle: Dom, 触发浮层的button
    ref: String, 来源
    openinfo: Object,  openInfo.androidArgs = '&page=videoMessageBoard&id=' + postid;
                       openInfo.iosArgs = '&page=videoMessageBoard&id=' + postid;
                       openInfo.GA_category = 'videoMessageBoard';
    callback: 回调函数， 可为空
*/
function downloadBox(bgImageUrl, clickEle, ref, openInfo, callback) {
    this.ref = ref;
    this.openInfo = openInfo || {};
    this.bgImageUrl = bgImageUrl;
    this.cb = callback || function () {};
    this.parentEle = '';
    this.init();
    this.attachEvent();
    if (clickEle) {
        this.clickEle = clickEle;
        this.attachButtonEvent();
    } else {
		if (/iphone/i.test(navigator.userAgent) && typeof WeixinJSBridge === 'object' && typeof WeixinJSBridge.invoke === 'function') {
            openJooxInWeixin(ref, openInfo);
        } else {
			this.parentEle.style.display = 'block';
        }
    }
}

downloadBox.prototype.init = function () {
    this.parentEle = document.createElement('div');
    this.parentEle.className = 'box-download';
    var html = '';
    html = '<span class="close"></span>'
            + '<div class="logo">'
                + '<span class="pic"></span>'
            + '</div>'
            + '<div class="btn-line">'
                + "<button class='but box-open'>I\'ve installed JOOX. Open Now!></button>"
                + "<button class='but box-install'>Download JOOX</button>"
            + '</div>'
            + '<div class="bg" style="background-image: url(' + this.bgImageUrl + ')"></div>'
            + '<div class="bg_mask"></div>';
    this.parentEle.innerHTML = html;
    document.querySelector('body').appendChild(this.parentEle);  
}

downloadBox.prototype.attachButtonEvent = function () {
	this.clickEle.addEventListener('touchend', function () {
		event.stopPropagation();
        cb();
		if (/iphone/i.test(navigator.userAgent) && typeof WeixinJSBridge === 'object' && typeof WeixinJSBridge.invoke === 'function') {
            openJooxInWeixin(ref, openInfo);
        } else {
			parent.style.display = 'block';
        }
	});
}

downloadBox.prototype.attachEvent = function () {
	var close = this.parentEle.querySelector('.close');
	var open = this.parentEle.querySelector('.box-open');
	var install = this.parentEle.querySelector('.box-install');
    var parent = this.parentEle;
    var ref = this.ref;
    var openInfo = this.openInfo;
    var cb = this.cb;

	close.addEventListener('touchend', function (event) {
		event.stopPropagation();
        parent.style.display = 'none';
        document.getElementById('content').style.pointerEvents = 'auto'
    }, false);
        
    open.addEventListener('touchend', function(event) {
        event.stopPropagation();
        openBox(ref, openInfo);
    });

    install.addEventListener('touchend', function(event) {
        event.stopPropagation();
        installBox(ref, openInfo);
    });
}

function openBox(ref, json) {
	var scheme = 'wemusic://www.joox.com?from=html5page&platform=';
    var GA =  json['GA_category']; 
    var event =  json.event || 'openClient_box';
    var platform = getPlatform();   
	if(platform === 'iOS') {
		scheme += 'ios' + json['iosArgs'];
	} else {
		scheme += 'android' + json['androidArgs'];
	}
	if(typeof ga === 'function') ga('send', 'event', GA, platform, event);
	//if (navigator.userAgent.indexOf('FBAN') > -1) 
	scheme = decodeURIComponent(scheme);
    launchApp(scheme);
}

function installBox (ref, json) {
	var GA =  json['GA_category']; 
    var event =  json.event || 'openMarket_box';
	var platform = getPlatform();
	var downloadUrl = 'https://www.joox.com/d?ref=' + ref;
	if(typeof ga === 'function') ga('send', 'event', GA, platform, event);
	
	var langCode,
        area = getUrlParam(location.href, 'area');

    if (area) {
        langCode = window.location.pathname.split('/')[1].substring(0,2) + (area ? ("_" + area) : "");
    } else {
        langCode = window.location.pathname.split('/')[1];
    }
    downloadUrl = downloadUrl + (langCode ? ('&lang=' + langCode) : '');
	location.href = downloadUrl;
}

function getPlatform () {
    var ua = navigator.userAgent,
        platform = 'other';
    if (/android/i.test(ua)) {
        platform = 'android';
    } else if (/iphone/i.test(ua)) {
        platform = 'iOS';
    }
    return platform;
} 

function getUrlParam(url, name) {

    var r = new RegExp('(\\?|#|&)' + name + '=([^&#\\?]*)(&|#|$|\\?)', 'i');
    var m = url.match(r);
    if ((!m || m === ''))
        m = top.location.href.match(r);

    return (!m ? '' : m[2]);
}

function openJooxInWeixin(ref, openInfo) {

    var downloadUrl = 'https://www.joox.com/d?ref=' + ref,
        scheme = 'wemusic://www.joox.com?from=html5page&platform=',
        ua = navigator.userAgent,
        platform = 'other';
        
    var event =  openInfo.event || 'openClient';

    var langCode,
        area = getUrlParam(location.href, 'area');

    if (area) {
        langCode = window.location.pathname.split('/')[1].substring(0,2) + (area ? ("_" + area) : "");
    } else {
        langCode = window.location.pathname.split('/')[1];
    }

    if (/android/i.test(ua)) {
        scheme += 'android' + openInfo.androidArgs,
            platform = 'android';
    } else if (/iphone/i.test(ua)) {
        scheme += 'ios' + openInfo.iosArgs,
            platform = 'iOS';
    }
  	
    ga('send', 'event', openInfo.GA_category, platform, event);
    setTimeout(function() {
        launchApp(scheme, ref + (langCode ? ('&lang=' + langCode) : ''));
    }, 200);
       
}


